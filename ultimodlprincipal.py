#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
import pathlib

class dlgPrincipal():
    def __init__(self, titulo=""):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("./ui/bio.ui")
        self.dialogo = self.builder.get_object("chooserDialog")
        self.dialogo.set_title("JAJA")
        self.dialogo.resize(600, 400)
        print("Se abrio ventana auxiliar")

# FILTRO
        filter = self.builder.get_object("filefilter")
        self.dialogo.add_filter(filter)
# BOTONES
        # boton cancelar se define con las condiciones cuando ocurren
        boton_cancelar = self.dialogo.add_button(Gtk.STOCK_CANCEL,
                                                 Gtk.ResponseType.CANCEL)
        # si es verdadero se mostrara la imagen si esta disponible
        boton_cancelar.set_always_show_image(True)
        # si el boton cancelar es clickeado, se llama al boton con la accion
        boton_cancelar.connect("clicked", self.boton_cancelar_clicked)

        # boton aceptar se define con las condiciones
        boton_aceptar = self.dialogo.add_button(Gtk.STOCK_OK,
                                                Gtk.ResponseType.OK)
        # si es verdadero se mostrara la imagen si esta disponible
        boton_aceptar.set_always_show_image(True)
        # si el boton aceptar es clickeado, se llama al boton con la accion
        boton_aceptar.connect("clicked", self.boton_aceptar_clicked)

    def boton_aceptar_clicked(self, btn=None):
        print("Presionaste botón aceptar")
        # Obtiene directorio seleccionado.
        ruta = self.dialogo.get_current_folder()
        return ruta

    def boton_cancelar_clicked(self, btn=None):
        print("presionaste el boton cancelar")