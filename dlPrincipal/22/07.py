#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from biopandas.pdb import PandasPdb


class dlgInformation():
    def __init__(self, title="", path=""):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("./ui/bio.ui")
        self.path = path
        # Ventana de diálogo.
        self.dialog = self.builder.get_object("proteinViewer")
        self.dialog.resize(800, 600)

        # Botón guardar.
        button_save = self.builder.get_object("buttonSave")
        button_save.connect("clicked", self.button_save_clicked)

        # Botón cancelar.
        button_cancel = self.builder.get_object("buttonCancel")
        button_cancel.connect("clicked", self.button_cancel_clicked)

        #COMBOBOX
        self.combobox = self.builder.get_object("combobox")
        render_text = Gtk.CellRendererText()
        self.combobox.pack_start(render_text, True)
        self.combobox.add_attribute(render_text, "text", 0)
        self.model = Gtk.ListStore(str)
        self.combobox.connect("changed", self.combobox_changed)

        list = ["ATOM", "HETATM", "ANISOU","OTHERS"]
        for index in list:
            self.model.append([index])
        self.combobox.set_model(self.model)
        self.data = self.builder.get_object("infoProtein")


# control de lo que ve el usuario
def combobox_changed(self, cmb=None):
        it = self.combobox.get_active_iter()
        model = self.combobox.get_model()
        value = model[it][0]
        print(value)
        ppdb = PandasPdb()
        ppdb.read_pdb(self.path)
        # print(ppdb.df[value].head())
        self.data.set_text(ppdb.df[value].head())

        # button_cancel = self.dialog.add_button(Gtk.STOCK_CANCEL,
        #                                         Gtk.ResponseType.CANCEL)
        #
        # button_cancel.set_always_show_image(True)
        # button_cancel.connect("clicked", self.button_cancel_clicked)´

    def button_save_clicked(self, btn=None):
        pass

    def button_cancel_clicked(self, btn=None):
        print("Cancelar - dlgInformation")
        self.dialog.destroy()