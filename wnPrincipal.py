#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from dlgPrincipal import dlgPrincipal

# Se genera la clase principal en este caso de la ventana
class wnPrincipal():
    def __init__(self):
        self.builder = Gtk.Builder()
        # Se obtiene el archivo
        self.builder.add_from_file("./ui/bio.ui")
        self.window = self.builder.get_object("wnPrincipal")
        self.window.connect("destroy", Gtk.main_quit)
        #CAMBIAR titulo
        self.window.set_title("TITULO")
        self.window.maximize()
        self.window.show_all()
        # BOTONES
        boton = self.builder.get_object("buttonOpen")
        boton.connect("clicked", self.boton_clicked)
        # LABEL
        self.label = self.builder.get_object("directory")
#FUNCION CUANDO SE SELECCIONA BOTON
    def boton_clicked(self, btn=None):
        print("Presione el boton")
        file_chooser = dlgPrincipal()
        response = file_chooser.dialogo.run()
        file_chooser.dialogo.destroy()

if __name__ == "__main__":
    PRINCIPAL = wnPrincipal()
    Gtk.main()
