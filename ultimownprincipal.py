#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gi
import pathlib
import os
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from dlgPrincipal import dlgPrincipal
from biopandas . pdb import PandasPdb


class wnPrincipal:
    def __init__(self):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("./ui/bio.ui")

        self.window = self.builder.get_object("wnPrincipal")
        self.window.connect("destroy", Gtk.main_quit)
        self.window.set_title("VISUALIZADOR ARCHIVOS PDB")
        self.window.maximize()
        self.window.show_all()

        # Label directorio.
        self.label = self.builder.get_object("directory")

        # Botones.
        # Cargar botón abrir.
        boton_abrir = self.builder.get_object("buttonOpen")
        boton_abrir.connect("clicked", self.boton_abrir_clicked)

        # Cargar botón ver.
        boton_ver = self.builder.get_object("view")
        # boton_ver.connect("clicked", self.boton_ver_clicked)

        # Cargar botón acerca.
        boton_acerca = self.builder.get_object("about")
        # boton_acerca.connect("clicked", self.boton_acerca_cliked)

        # Liststore.
        self.liststore = self.builder.get_object("tree")
        # self.liststore.connect("cursor-changed", self.liststore_changed)
        self.model = Gtk.ListStore(*(1 * [str]))
        self.liststore.set_model(model=self.model)

        cell = Gtk.CellRendererText()

        column = Gtk.TreeViewColumn(title="Archivo",
                                    cell_renderer=cell,
                                    text=0)

        self.liststore.append_column(column)


    # def liststore_changed():
    #     model, it = self.liststore.get_selection().get_selected()
    #     if model is None or it is None:
    #         return
    #
    #     nombre = model.get_value(it, 0)
    #     nombre = "".join(["Nombre: ",
    #                       nombre])
    #     apellido = model.get_value(it, 1)
    #     self.label_changed.set_text(nombre + apellido)


    def boton_abrir_clicked(self, btn=None):
        file_chooser = dlgPrincipal(titulo="Seleccionar directorio")
        response = file_chooser.dialogo.run()

        if response == Gtk.ResponseType.OK:

            ruta = file_chooser.boton_aceptar_clicked()

            # Se filtran archivos de formato .pdb.
            lista = list(pathlib.Path(ruta).glob('*.pdb'))

            if lista != []:

                for i in lista:
                    texto = os.path.split(i)
                    self.model.append([str(texto[1])])
                #     ppdb = PandasPdb()
                #     ppdb.read_pdb(i)
                #     print(ppdb.df.keys())
                #     print("\nRaw PDB file contents:\n\n%s\n..." %ppdb.pdb_text[:1000])
                #     print(ppdb.df["ATOM"])
                #     ppdb.to_pdb(path="./data/11gs_ATOM.pdb",
                #                 records=["ATOM"],
                #                 gz=False,
                #                 append_newline=True)

                # for i in lista:
                #     print(i)

            else:
                print("AQUÍ PUEDE IR UN MESSAGEDIALOG INFO indicando que no se cargó nada")

        file_chooser.dialogo.destroy()


if __name__ == "__main__":
    PRINCIPAL = wnPrincipal()
    Gtk.main()
