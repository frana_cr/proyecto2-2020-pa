#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ORDENAR ESTO!!!!!
import pathlib
import os

import __main__
# Pymol : quiet and no GUI
__main__.pymol_argv = ["pymol", "-qc"]
import pymol
pymol.finish_launching()

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

from biopandas.pdb import PandasPdb
from dlgFileChooser import dlgFileChooser
from dlgInformation import dlgInformation

folder_path = None

class wnPrincipal():
    def __init__(self):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("./ui/bio.ui")

        self.window = self.builder.get_object("wnPrincipal")
        self.window.connect("destroy", Gtk.main_quit)
        self.window.set_title("VISUALIZADOR ARCHIVOS PDB")
        self.window.resize(800, 600)
        self.window.show_all()

        # Label directorio.
        # self.dir = self.builder.get_object("directory")

        # Label info.
        self.info = self.builder.get_object("info")

        # Imagen.
        self.imagen = self.builder.get_object("image")
        # self.imagen.clear()

        # Botón para abrir directorio.
        button_open = self.builder.get_object("buttonOpen")
        button_open.connect("clicked", self.button_open_clicked)

        # Botón para visualizar archivo.
        button_view = self.builder.get_object("view")
        button_view.connect("clicked", self.button_view_clicked)

        # Botón información.
        button_about = self.builder.get_object("about")
        # boton_acerca.connect("clicked", self.boton_acerca_cliked)

        # Liststore.
        self.liststore = self.builder.get_object("tree")
        self.model = Gtk.ListStore(*(1 * [str]))
        self.liststore.set_model(model=self.model)

        cell = Gtk.CellRendererText()

        column = Gtk.TreeViewColumn(title="Archivo(s) PDB",
                                    cell_renderer=cell,
                                    text=0)

        self.liststore.append_column(column)

        self.selection = self.liststore.get_selection()
        self.selection.connect("changed", self.on_changed)

    def on_changed(self, selection):
        model, it = selection.get_selected()
        if it is not None:
            # Obtener ruta del archivo.
            file = "/".join([folder_path, model[it][0]])
            # Cargar texto de visualización previa.
            ppdb = PandasPdb()
            ppdb.read_pdb(file)
            text = ppdb.pdb_text[:1000]
            self.info.set_text(text)

            # Cargar imagen de la proteína.
            name = model[it][0].split(".")
            self.imagen.set_from_file("".join(["./",str(name[0]), ".png"]))

    def button_view_clicked(self, btn=None):
        # Visualiza información sobre un archivo seleccionado.
        model, it = self.liststore.get_selection().get_selected()
        if model is None or it is None:
            return
        variable = "/".join([folder_path, model[it][0]])
        print(variable)
        dlg = dlgInformation(title="Info", path=variable)
        dlg.dialog.run()

    def button_open_clicked(self, btn=None):
        global folder_path
        file_chooser = dlgFileChooser(title="Seleccionar directorio")
        response = file_chooser.dialog.run()

        if response == Gtk.ResponseType.OK:
            # Se obtiene directorio seleccionado.
            folder_path = file_chooser.button_ok_clicked()
            # Se filtran archivos de formato .pdb presentes en el directorio.
            file_list = list(pathlib.Path(folder_path).glob('*.pdb'))

            if file_list != []:
                for f in file_list:
                    text = os.path.split(f)
                    # Generar imágenes de archivos en carpeta.
                    pdb_name = text[1].split(".")
                    pdb_name = pdb_name[0]
                    pdb_file = f
                    pymol.cmd.load(pdb_file,pdb_name)
                    pymol.cmd.disable("all")
                    pymol.cmd.enable(pdb_name)
                    pymol.cmd.hide("all")
                    pymol.cmd.show("cartoon")
                    pymol.cmd.set("ray_opaque_background", 0)
                    pymol.cmd.pretty(pdb_name)
                    pymol.cmd.png("%s.png" % (pdb_name))

                    # Añadir nombre de archivo a la ventana.
                    self.model.append([str(text[1])])
            else:
                # Reiniciar cuando el directorio no tiene archivos pdb.
                self.model = Gtk.ListStore(*(1 * [str]))
                self.liststore.set_model(model=self.model)
                self.imagen.clear()
                self.info.set_text("")

            file_chooser.dialog.destroy()


if __name__ == "__main__":
    PRINCIPAL = wnPrincipal()
    Gtk.main()